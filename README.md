# My-Thai-star-jml
This is a JML annotated version of the java project My thai star developed with Devonfw. 

Some annotations have been added to act as a preconditions,postconditions and result checks.

JML is a notation for formally specifying the behavior and interfaces of Java classes and methods.

## Used JML annotations

-requires: defines a precondition in the following method

-ensures: defines a postcondition in the following method

-also: for declaring that a method is overriding parent class method

-assert: JML assertion

-spec_public: declares a private or protected variable as public in terms of JML specification.

-\result: declares the value of return of a method

-non_null: invariant to declare somethinf can't be null


## Interfaces and Classes with JML annotations
-BookingmanagementRestService

-BookingmanagementImpl

-UsermanagementRestService

-UsermanagementImpl

-PredictionmanagementRestService

-PredictionmanagementImpl

-OrdermanagementRestService

-OrdermanagementImpl

-Mail

-MailImpl

-ImagemanagementRestService

-ImagemanagementImpl

-DishmanagementRestService

-DishmanagementImpl

-ClustermanagementRestService

-ClustermanagementImpl

